package com.example.demo.controller;

import com.example.demo.dto.PacientViewDTO;
import com.example.demo.dto.PatientDTO;
import com.example.demo.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(value = "http://localhost:4200")
@RequestMapping(value = "/patient")
public class PatientController {

    private final PatientService patientService;

    @Autowired
    public PatientController(PatientService patientService) {
        this.patientService = patientService;
    }

    @PostMapping()
    public Long createPatient(@RequestBody PatientDTO patientDTO) {
        return patientService.createPatient(patientDTO);
    }

    @GetMapping(value = "getDoctor/{doctorUsername}")
    public List<PatientDTO> getAllPatientsByDoctorId(@PathVariable("doctorUsername") String doctorUsername){
        return patientService.getAllPatientsByDoctorUsername(doctorUsername);
    }

    @GetMapping(value = "/{id}")
    public PatientDTO getPatient(@PathVariable("id") Long id){
        return patientService.findPatientById(id);
    }

    @GetMapping(value = "/get/{caregiverUsername}")
    public List<PacientViewDTO> getAllPatientsByCaregiverId(@PathVariable("caregiverUsername") String caregiverUsername){
        return patientService.getAllPatientsByCaregiverUsername(caregiverUsername);
    }


    @PutMapping(value = "/{id}")
    public Long updatePatient(@RequestBody PatientDTO patientDTO,@PathVariable("id") Long id){
        return patientService.updatePatient(patientDTO,id);
    }

    @DeleteMapping(value = "/{id}")
    public Map<String, Boolean> deletePatient(@PathVariable("id") Long id){
        return patientService.deletePatient(id);
    }
}
