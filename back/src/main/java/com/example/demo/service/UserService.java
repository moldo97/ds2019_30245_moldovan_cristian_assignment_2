package com.example.demo.service;

import com.example.demo.dto.CaregiverDTO;
import com.example.demo.dto.CaregiverViewDTO;
import com.example.demo.dto.LoginDTO;
import com.example.demo.dto.builders.CaregiverBuilder;
import com.example.demo.dto.builders.CaregiverViewBuilder;
import com.example.demo.dto.builders.LoginBuilder;
import com.example.demo.errorHandler.ResourceNotFoundException;
import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.GsonBuilderUtils;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public LoginDTO login(LoginDTO loginDTO){
        User user = userRepository.findByUsername(loginDTO.getUsername());

        if(user == null)
            return null;
        else if(!user.getPassword().equals(loginDTO.getPassword()))
            return null;

        return LoginBuilder.generateDTOFromEntity(user);
    }


    ///////////////////////////////////////////////////////////
    //Caregiver part

    //find one caregiver by id
    public CaregiverDTO findUserById(Long id){
        Optional<User> user = userRepository.findById(id);

        if(!user.isPresent()){
            throw new ResourceNotFoundException("User","id",id);
        }

        return CaregiverBuilder.generateDTOFromEntity(user.get());
    }

    //find one user by username
    public CaregiverViewDTO findUserByUsername(String username){
        User user = userRepository.findByUsername(username);

        if(user == null){
            throw new ResourceNotFoundException("User","username",username);
        }

        return CaregiverViewBuilder.generateDTOFromEntity(user);
    }


    //find all caregivers
    public List<CaregiverDTO> findAllCaregiver(){
        List<User> users = userRepository.findAll();

        return users.stream()
                .filter(user -> user.getRole().toString().equals("CAREGIVER"))
                .map(user -> CaregiverBuilder.generateDTOFromEntity(user))
                .collect(Collectors.toList());
    }

    //create caregiver
    public Long create(CaregiverDTO caregiverDTO){
        return userRepository
                .save(CaregiverBuilder.generateEntityFromDto(caregiverDTO))
                .getId();
    }

    //update caregiver
    public Long update(CaregiverViewDTO caregiverViewDTO, Long id){
        Optional<User> user = userRepository.findById(id);

        if(!user.isPresent()){
            throw new ResourceNotFoundException("User","id",id);
        }

        User updateUser = CaregiverViewBuilder.generateEntityFromDTO(caregiverViewDTO);
        updateUser.setId(id);

        return userRepository
                .save(updateUser)
                .getId();
    }

    //delete caregiver
    public Map<String, Boolean> delete(Long id){
        User user = userRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("User","id",id));

        userRepository.delete(user);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }

    ///////////////////////////////////////////////////////////



}
