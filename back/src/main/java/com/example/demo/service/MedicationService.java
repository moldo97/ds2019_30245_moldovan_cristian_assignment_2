package com.example.demo.service;

import com.example.demo.dto.MedicationDTO;
import com.example.demo.dto.MedicationViewDTO;
import com.example.demo.dto.builders.MedicationBuilder;
import com.example.demo.dto.builders.MedicationViewBuilder;
import com.example.demo.errorHandler.ResourceNotFoundException;
import com.example.demo.model.Medication;
import com.example.demo.repository.MedicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MedicationService {

    private final MedicationRepository medicationRepository;

    @Autowired
    public MedicationService(MedicationRepository medicationRepository){
        this.medicationRepository = medicationRepository;
    }

    //find one medication by id
    public MedicationDTO findMedicationById(Long id){
        Optional<Medication> medication = medicationRepository.findById(id);

        if(!medication.isPresent()){
            throw new ResourceNotFoundException("Medication","id",id);
        }

        return MedicationBuilder.generateDTOFromEntity(medication.get());
    }

    //find all medication
    public List<MedicationDTO> findAllMedication(){
        List<Medication> medications = medicationRepository.findAll();

        return medications.stream()
                .map(medication-> MedicationBuilder.generateDTOFromEntity(medication))
                .collect(Collectors.toList());
    }

    //create medication
    public Long create(MedicationDTO medicationDTO){
        return medicationRepository
                .save(MedicationBuilder.generateEntityFromDTO(medicationDTO))
                .getId();
    }

    //update medication
    public Long update(MedicationViewDTO medicationViewDTO,Long id){
        Optional<Medication> medication = medicationRepository.findById(id);

        if(!medication.isPresent()){
            throw new ResourceNotFoundException("Medication","id",id);
        }

        Medication updateMedication = MedicationViewBuilder.generateEntityFromDTO(medicationViewDTO);
        updateMedication.setId(id);

        return medicationRepository
                .save(updateMedication)
                .getId();

    }

    //delete medication
    public Map<String , Boolean> delete(Long id){
        Medication medication = medicationRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Medication","id",id));

        medicationRepository.delete(medication);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }

}
