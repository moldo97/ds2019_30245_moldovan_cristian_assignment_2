package com.example.demo.dto;

import java.util.Date;

public class ActivityDTO {
    private Long id;
    private Date startTime;
    private Date endTime;
    private String activity;
    private Long patientId;

    public ActivityDTO(Date startTime, Date endTime, String activity, Long patientId) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.activity = activity;
        this.patientId = patientId;
    }

    public ActivityDTO(Long id, Date startTime, Date endTime, String activity, Long patientId) {
        this.id = id;
        this.startTime = startTime;
        this.endTime = endTime;
        this.activity = activity;
        this.patientId = patientId;
    }

    public ActivityDTO(){

    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPatientId() {
        return patientId;
    }

    public void setPatientId(Long patientId) {
        this.patientId = patientId;
    }

    @Override
    public String toString() {
        return "Received Activity{" +
                "startTime=" + startTime +
                ", endTime=" + endTime +
                ", activity='" + activity + '\'' +
                ", patientId=" + patientId +
                '}';
    }
}
