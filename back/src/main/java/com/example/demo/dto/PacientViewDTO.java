package com.example.demo.dto;

import com.example.demo.enums.Gender;
import com.example.demo.enums.Role;

import java.util.Date;

public class PacientViewDTO {
    private Role role;
    private String name;
    private Date birthDate;
    private String address;
    private Gender gender;
    private String medicalRecord;

    public PacientViewDTO(Role role, String name, Date birthDate, String address, Gender gender, String medicalRecord) {
        this.role = role;
        this.name = name;
        this.birthDate = birthDate;
        this.address = address;
        this.gender = gender;
        this.medicalRecord = medicalRecord;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }
}
