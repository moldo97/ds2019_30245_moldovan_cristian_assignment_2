package com.example.demo.dto;

public class MedicationViewDTO {
    private String name;
    private String sideEffects;
    private Double dossage;

    public MedicationViewDTO(){

    }

    public MedicationViewDTO(String name, String sideEffects, Double dossage) {
        this.name = name;
        this.sideEffects = sideEffects;
        this.dossage = dossage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(String sideEffects) {
        this.sideEffects = sideEffects;
    }

    public Double getDossage() {
        return dossage;
    }

    public void setDossage(Double dossage) {
        this.dossage = dossage;
    }
}
