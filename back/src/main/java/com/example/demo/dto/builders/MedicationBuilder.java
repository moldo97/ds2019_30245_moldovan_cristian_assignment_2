package com.example.demo.dto.builders;

import com.example.demo.dto.MedicationDTO;
import com.example.demo.model.Medication;

public class MedicationBuilder {

    private MedicationBuilder(){

    }

    public static MedicationDTO generateDTOFromEntity(Medication medication){
        return new MedicationDTO(medication.getId(),
                medication.getName(),
                medication.getSideEffects(),
                medication.getDosage());
    }

    public static Medication generateEntityFromDTO(MedicationDTO medicationDTO){
        return new Medication(medicationDTO.getId(),
                medicationDTO.getName(),
                medicationDTO.getSideEffects(),
                medicationDTO.getDossage());
    }
}
