package com.example.demo.dto.builders;

import com.example.demo.dto.PacientViewDTO;
import com.example.demo.model.Patient;

public class PacientViewBuilder {

    public PacientViewBuilder(){

    }

    public static PacientViewDTO generateDTOFromEntity(Patient patient){
        return new PacientViewDTO(patient.getRole(),
                patient.getName(),
                patient.getBirthDate(),
                patient.getAdress(),
                patient.getGender(),
                patient.getMedicalRecord());
    }
}
