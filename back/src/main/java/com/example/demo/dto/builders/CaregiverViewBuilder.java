package com.example.demo.dto.builders;

import com.example.demo.dto.CaregiverViewDTO;
import com.example.demo.model.User;

public class CaregiverViewBuilder {

    private CaregiverViewBuilder(){

    }

    public static CaregiverViewDTO generateDTOFromEntity(User user){
        return new CaregiverViewDTO(user.getId(),
                user.getRole(),
                user.getUsername(),
                user.getPassword(),
                user.getName(),
                user.getBirthDate(),
                user.getAdress(),
                user.getGender());
    }

    public static User generateEntityFromDTO(CaregiverViewDTO caregiverViewDTO){
        return new User(caregiverViewDTO.getId(),
                caregiverViewDTO.getUsername(),
                caregiverViewDTO.getPassword(),
                caregiverViewDTO.getRole(),
                caregiverViewDTO.getName(),
                caregiverViewDTO.getBirthDate(),
                caregiverViewDTO.getAddress(),
                caregiverViewDTO.getGender());
    }
}
