package com.example.demo.activity;

import com.example.demo.dto.ActivityDTO;
import com.example.demo.dto.builders.ActivityBuilder;
import com.example.demo.errorHandler.ResourceNotFoundException;
import com.example.demo.model.Activity;
import com.example.demo.model.Patient;
import com.example.demo.repository.ActivityRepository;
import com.example.demo.repository.PatientRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Optional;

@Service
public class ConsumerService {
    private final ActivityRepository activityRepository;
    private final PatientRepository patientRepository;
    private final SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    public ConsumerService(ActivityRepository activityRepository, PatientRepository patientRepository, SimpMessagingTemplate simpMessagingTemplate) {
        this.activityRepository = activityRepository;
        this.patientRepository = patientRepository;
        this.simpMessagingTemplate = simpMessagingTemplate;
        this.activityRepository.deleteAll();
    }

    @RabbitListener(queues = "activities")
    public void receive(byte[] in){
        ObjectMapper mapper = new ObjectMapper();
        DateFormat dateFormat;
        dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        mapper.setDateFormat(dateFormat);
        try {
            ActivityDTO activityDTO = mapper.readValue(new String(in), ActivityDTO.class);
            System.out.println(activityDTO.toString());
            processActivities(activityDTO);
            createActivity(activityDTO);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public Long createActivity(ActivityDTO activityDTO){
        Optional<Patient> patient = patientRepository.findById(activityDTO.getPatientId());
        Activity activity =  ActivityBuilder.generateEntityFromDTO(activityDTO);

        if(!patient.isPresent()){
            throw new ResourceNotFoundException("Patient","id",activityDTO.getPatientId());
        }

        activity.setPatient(patient.get());

        return activityRepository
                .save(activity)
                .getId();
    }

    private void processActivities(ActivityDTO activityDTO){
        final int MILLI_TO_HOUR = 1000 * 60 * 60;
        long hours = (activityDTO.getEndTime().getTime() - activityDTO.getStartTime().getTime()) / MILLI_TO_HOUR;
        System.out.println(hours);

        if((activityDTO.getActivity().equals("Sleeping")
                || activityDTO.getActivity().equals("Leaving"))
                && hours>12){
            simpMessagingTemplate.convertAndSend("/topic/caregiver","Patient with id: " + activityDTO.getPatientId() + " " +  activityDTO.getStartTime() + " has problems");
        }
        if((activityDTO.getActivity().equals("Toileting")
                || activityDTO.getActivity().equals("Showering"))
                && hours>1){
            simpMessagingTemplate.convertAndSend("/topic/caregiver","Patient with id: " + activityDTO.getPatientId() + " " + activityDTO.getStartTime() + " has problems");
        }
    }
}
