import org.omg.PortableServer.THREAD_POLICY_ID;

import java.sql.Timestamp;
import java.util.Date;

public class Activity {
    private Long patientId;
    private Timestamp startTime;
    private Timestamp endTime;
    private String activity;

    public Activity(Long patientId, Timestamp startTime, Timestamp endTime, String activity) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.activity = activity;
        this.patientId = patientId;
    }

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public Long getPatientId() {
        return patientId;
    }

    public void setPatientId(Long patientId) {
        this.patientId = patientId;
    }
}
