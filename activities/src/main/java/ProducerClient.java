import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Timestamp;

public class ProducerClient {
    private final static String QUEUE_NAME = "activities";
    private final static String filePath = "activity.txt";

    static FileReader input;

    static {
        try {
            input = new FileReader(filePath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    static BufferedReader reader = new BufferedReader(input);
    static String myLine = null;

    public ProducerClient() throws FileNotFoundException {
    }

    public static void main(String[] argv) throws Exception {

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.queueDeclare(QUEUE_NAME, false, false, false, null);
            while( (myLine = reader.readLine()) != null){
                String[] line = myLine.split(",");
                Long patientId = Long.parseLong(line[0]);
                Timestamp startTime = DateUtil.convertStringToTimestamp(line[1],"yyyy-MM-dd hh:mm:ss");
                Timestamp endTime = DateUtil.convertStringToTimestamp(line[2],"yyyy-MM-dd hh:mm:ss");
                String activity = line[3];

                Activity activityQueue = new Activity(patientId,startTime,endTime,activity);

                ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
                String jsonActivity = ow.writeValueAsString(activityQueue);

                channel.basicPublish("", QUEUE_NAME, null, jsonActivity.getBytes("UTF-8"));
                System.out.println("Sent " + jsonActivity);
                Thread.sleep(1000);
            }


        }
    }
}
