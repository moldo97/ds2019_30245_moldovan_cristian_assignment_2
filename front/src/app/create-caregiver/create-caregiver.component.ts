import {Component, OnInit} from '@angular/core';
import {Medication} from '../models/medication';
import {MedicationService} from '../service/medication.service';
import {Router} from '@angular/router';
import {CaregiverService} from '../service/caregiver.service';
import {User} from '../models/user';

@Component({
  selector: 'app-create-caregiver',
  templateUrl: './create-caregiver.component.html',
  styleUrls: ['./create-caregiver.component.css']
})
export class CreateCaregiverComponent implements OnInit {

  user: User = new User();
  submitted = false;

  constructor(private caregiverService: CaregiverService,
              private router: Router) {
  }

  ngOnInit() {
  }

  save() {
    this.user.role = 'CAREGIVER';
    this.caregiverService.createCaregiver(this.user)
      .subscribe(
        data => {
          console.log(data);
          alert('Caregiver was created successfully');
        },
        error => console.log(error));
    this.user = new User();
    this.gotoList();
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

  gotoList() {
    this.router.navigate(['doctor']);
  }

}
