import { Injectable } from '@angular/core';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';

@Injectable({
  providedIn: 'root'
})
export class WebsocketApiService {

  webSocketEndPoint = 'http://localhost:8080/ws';

  constructor() {
  }

  _connect() {
    const ws = new SockJS(this.webSocketEndPoint);
    return Stomp.over(ws);
  }
}
