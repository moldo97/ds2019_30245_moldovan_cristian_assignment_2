import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {CreateMedicationComponent} from './create-medication/create-medication.component';
import {MedicationListComponent} from './medication-list/medication-list.component';
import {MedicationDetailsComponent} from './medication-details/medication-details.component';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {UpdateMedicationComponent} from './update-medication/update-medication.component';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app.routing.module';
import {HttpClientModule} from '@angular/common/http';
import {DoctorPageComponent} from './doctor-page/doctor-page.component';
import {LoginComponent} from './login/login.component';
import {CaregiverComponent} from './caregiver-page/caregiver.component';
import {PatientPageComponent} from './patient-page/patient-page.component';
import {CaregiverListComponent} from './caregiver-list/caregiver-list.component';
import {CaregiverDetailsComponent} from './caregiver-details/caregiver-details.component';
import {CreateCaregiverComponent} from './create-caregiver/create-caregiver.component';
import {UpdateCaregiverComponent} from './update-caregiver/update-caregiver.component';
import {CaregivePatientListComponent} from './caregive-patient-list/caregive-patient-list.component';
import {PatientListComponent} from './patient-list/patient-list.component';
import {PatientDetailsComponent} from './patient-details/patient-details.component';
import {UpdatePatientComponent} from './update-patient/update-patient.component';
import {CreatePatientComponent} from './create-patient/create-patient.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    CaregiverComponent,
    DoctorPageComponent,
    PatientPageComponent,
    CreateMedicationComponent,
    MedicationDetailsComponent,
    UpdateMedicationComponent,
    MedicationListComponent,
    CaregiverListComponent,
    CaregiverDetailsComponent,
    CreateCaregiverComponent,
    UpdateCaregiverComponent,
    CaregivePatientListComponent,
    PatientListComponent,
    PatientDetailsComponent,
    UpdatePatientComponent,
    CreatePatientComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    CommonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
