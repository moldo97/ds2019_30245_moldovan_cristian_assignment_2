import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {WebsocketApiService} from '../websocket/websocket-api.service';

@Component({
  selector: 'app-caregiver',
  templateUrl: './caregiver.component.html',
  styleUrls: ['./caregiver.component.css']
})
export class CaregiverComponent implements OnInit {
  name: string;
  greetings = [];
  topic = '/topic/caregiver';

  constructor(private router: Router, private webSocket: WebsocketApiService) {
  }

  ngOnInit() {
    this.name = sessionStorage.getItem('name');
    const stompClient = this.webSocket._connect();
    const thisJr = this;
    stompClient.connect({}, frame => {
      stompClient.subscribe(thisJr.topic, sdkEvent => {
        thisJr.onMessageReceived(sdkEvent);
      });
    });
  }

  handleMessage(message) {
    this.greetings.push(message);
  }

  onMessageReceived(message) {
    console.log('Message Received from Server :: ' + message);
    this.handleMessage(JSON.stringify(message.body));
  }

  logout() {
    this.router.navigate(['login']);
    sessionStorage.setItem('role', '');
    sessionStorage.setItem('name', '');
    sessionStorage.setItem('username', '');
  }
}
