import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaregivePatientListComponent } from './caregive-patient-list.component';

describe('CaregivePatientListComponent', () => {
  let component: CaregivePatientListComponent;
  let fixture: ComponentFixture<CaregivePatientListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaregivePatientListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaregivePatientListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
