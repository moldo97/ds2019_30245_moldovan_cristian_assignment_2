import {Component, OnInit} from '@angular/core';
import {PatientService} from '../service/patient.service';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {Patient} from '../models/patient';

@Component({
  selector: 'app-caregive-patient-list',
  templateUrl: './caregive-patient-list.component.html',
  styleUrls: ['./caregive-patient-list.component.css']
})
export class CaregivePatientListComponent implements OnInit {
  patient: Observable<Patient[]>;

  constructor(private patientService: PatientService,
              private router: Router) {
  }

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.patient = this.patientService.getPatientList(sessionStorage.getItem('username'));
  }

  back() {
    this.router.navigate(['/caregiver']);
  }

}
