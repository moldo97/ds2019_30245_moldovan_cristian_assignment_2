import { Component, OnInit } from '@angular/core';
import {User} from '../models/user';
import {ActivatedRoute, Router} from '@angular/router';
import {CaregiverService} from '../service/caregiver.service';
import {Patient} from '../models/patient';
import {PatientService} from '../service/patient.service';

@Component({
  selector: 'app-patient-details',
  templateUrl: './patient-details.component.html',
  styleUrls: ['./patient-details.component.css']
})
export class PatientDetailsComponent implements OnInit {

  id: number;
  patient: Patient;

  constructor(private route: ActivatedRoute, private router: Router,
              private patientService: PatientService,
              private caregiverService: CaregiverService) { }

  ngOnInit() {
    this.patient = new Patient();

    this.id = this.route.snapshot.params.id;

    this.patientService.getPatient(this.id)
      .subscribe(data => {
        console.log(data);
        this.patient = data;
      }, error => console.log(error));
  }

  list() {
    this.router.navigate(['doctor/patient']);
  }

}
