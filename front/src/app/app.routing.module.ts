
import {MedicationDetailsComponent} from './medication-details/medication-details.component';
import {CreateMedicationComponent} from './create-medication/create-medication.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MedicationListComponent} from './medication-list/medication-list.component';
import {UpdateMedicationComponent} from './update-medication/update-medication.component';
import {LoginComponent} from './login/login.component';
import {DoctorPageComponent} from './doctor-page/doctor-page.component';
import {CaregiverComponent} from './caregiver-page/caregiver.component';
import {DoctorGuardService} from './guards/doctor-guard.service';
import {CaregiverGuardService} from './guards/caregiver-guard.service';
import {PatientPageComponent} from './patient-page/patient-page.component';
import {PatientGuardService} from './guards/patient-guard.service';
import {CaregiverListComponent} from './caregiver-list/caregiver-list.component';
import {CaregiverDetailsComponent} from './caregiver-details/caregiver-details.component';
import {CreateCaregiverComponent} from './create-caregiver/create-caregiver.component';
import {UpdateCaregiverComponent} from './update-caregiver/update-caregiver.component';
import {CaregivePatientListComponent} from './caregive-patient-list/caregive-patient-list.component';
import {PatientListComponent} from './patient-list/patient-list.component';
import {PatientDetailsComponent} from './patient-details/patient-details.component';
import {CreatePatientComponent} from './create-patient/create-patient.component';
import {UpdatePatientComponent} from './update-patient/update-patient.component';


const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'doctor', component: DoctorPageComponent, canActivate: [DoctorGuardService]},
  { path: 'caregiver', component: CaregiverComponent, canActivate: [CaregiverGuardService]},
  { path: 'patient', component: PatientPageComponent, canActivate: [PatientGuardService]},
  { path: 'login', component: LoginComponent },
  { path: 'doctor/medication', component: MedicationListComponent},
  { path: 'doctor/addMedication', component: CreateMedicationComponent},
  { path: 'updateMedication/:id', component: UpdateMedicationComponent},
  { path: 'medicationDetails/:id', component: MedicationDetailsComponent},
  { path: 'doctor/caregiver', component: CaregiverListComponent},
  { path: 'caregiverDetails/:id', component: CaregiverDetailsComponent},
  { path: 'doctor/addCaregiver', component: CreateCaregiverComponent},
  { path: 'updateCaregiver/:id', component: UpdateCaregiverComponent},
  { path: 'doctor/patient', component: PatientListComponent},
  { path: 'patientDetails/:id', component: PatientDetailsComponent},
  { path: 'doctor/addPatient', component: CreatePatientComponent},
  { path: 'updatePatient/:id', component: UpdatePatientComponent},
  { path: 'caregiver/patient', component: CaregivePatientListComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
