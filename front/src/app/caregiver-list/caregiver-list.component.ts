import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {User} from '../models/user';
import {CaregiverService} from '../service/caregiver.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-caregiver-list',
  templateUrl: './caregiver-list.component.html',
  styleUrls: ['./caregiver-list.component.css']
})
export class CaregiverListComponent implements OnInit {
  caregiver: Observable<User[]>;

  constructor(private caregiverService: CaregiverService,
              private router: Router) { }

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.caregiver = this.caregiverService.getCaregiverList();
  }

  deleteCaregiver(id: number) {
    this.caregiverService.deleteCaregiver(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  caregiverDetails(id: number) {
    this.router.navigate(['caregiverDetails', id]);
  }

  updateCaregiver(id: number, user: User) {
    this.router.navigate(['updateCaregiver', id, user]);
  }

  back() {
    this.router.navigate(['/doctor']);
  }
}
