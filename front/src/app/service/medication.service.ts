import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {REST_API} from '../common/API';
import {Observable} from 'rxjs';
import {Medication} from '../models/medication';

@Injectable({
  providedIn: 'root'
})
export class MedicationService {

  constructor(private http: HttpClient) {
  }

  getMedication(id: number): Observable<any> {
    return this.http.get(`${REST_API + 'medication'}/${id}`);
  }

  createMedication(medication: Medication): Observable<any> {
    return this.http.post(`${REST_API + 'medication'}`, medication);
  }

  updateMedication(id: number, value: any): Observable<any> {
    return this.http.put(`${REST_API + 'medication'}/${id}`, value);
  }

  deleteMedication(id: number): Observable<any> {
    return this.http.delete(`${REST_API + 'medication'}/${id}`, {responseType: 'text'});
  }

  getMedicationList(): Observable<any> {
    return this.http.get(`${REST_API + 'medication'}`);
  }
}
