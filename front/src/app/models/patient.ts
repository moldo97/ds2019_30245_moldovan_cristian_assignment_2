export class Patient {
  id: number;
  caregiverId: number;
  doctorId: number;
  role: string;
  username: string;
  password: string;
  name: string;
  birthDate: Date;
  address: string;
  gender: string;
  medicalRecord: string;
}
