export enum Role {
  Doctor = 'Doctor',
  Caregiver = 'Caregiver',
  Patient = 'Patient'
}
