import {MedicationService} from '../service/medication.service';
import {Medication} from '../models/medication';
import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-create-medication',
  templateUrl: './create-medication.component.html',
  styleUrls: ['./create-medication.component.css']
})
export class CreateMedicationComponent implements OnInit {

  medication: Medication = new Medication();
  submitted = false;

  constructor(private medicationService: MedicationService,
              private router: Router) {
  }

  ngOnInit() {
  }

  newMedication(): void {
    this.submitted = false;
    this.medication = new Medication();
  }


  save() {
    this.medicationService.createMedication(this.medication)
      .subscribe(
        data => {
          console.log(data);
          alert('Medication was created successfully');
        },
        error => console.log(error));
    this.medication = new Medication();
    this.gotoList();
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

  gotoList() {
    this.router.navigate(['doctor']);
  }
}
