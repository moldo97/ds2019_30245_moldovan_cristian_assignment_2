import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';


@Component({
  selector: 'app-doctor-page',
  templateUrl: './doctor-page.component.html',
  styleUrls: ['./doctor-page.component.css']
})
export class DoctorPageComponent implements OnInit {
  name: string;

  constructor(private router: Router) { }

  ngOnInit() {
    this.name = sessionStorage.getItem('name');
  }

  logout() {
    this.router.navigate(['login']);
    sessionStorage.setItem('role', '');
    sessionStorage.setItem('name', '');
    sessionStorage.setItem('username', '');
  }
}
