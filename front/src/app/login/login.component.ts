import {Component, OnInit} from '@angular/core';
import {UserService} from '../service/user.service';
import {Router} from '@angular/router';
import {LoginDTO, User} from '../models/user';
import {Role} from '../models/role';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: LoginDTO = new LoginDTO();

  userDTO: User;

  role: Role;

  constructor(private userService: UserService, private router: Router) {
  }

  ngOnInit() {
  }

  login() {
    this.userService.login(this.user)
      .subscribe(data => {
        console.log(data);
        this.userService.getUser(this.user.username)
          .subscribe(data1 => {
            console.log(data1);
            this.userDTO = data1;
            sessionStorage.setItem('role', this.userDTO.role);
            sessionStorage.setItem('name', this.userDTO.name);
            sessionStorage.setItem('username', this.userDTO.username);
            this.checkRoles();
          }, error => {
            console.log(error);
            alert('Please enter a valid user');
          });
      }, (err) => console.log(err));


  }

  onSubmit() {
    this.login();
  }

  gotoDoctor() {
    this.router.navigate(['/doctor']);
  }

  goToCaregiver() {
    this.router.navigate(['/caregiver']);
  }

  goToPatient() {
    this.router.navigate(['/patient']);
  }

  checkRoles() {
    if (sessionStorage.getItem('role') === 'DOCTOR') {
      this.gotoDoctor();
    }
    if (sessionStorage.getItem('role') === 'CAREGIVER') {
      this.goToCaregiver();
    }
    if (sessionStorage.getItem('role') === 'PATIENT') {
      this.goToPatient();
    }
  }

}
